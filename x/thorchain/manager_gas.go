package thorchain

import (
	"gitlab.com/thorchain/thornode/v3/common"
)

type OutAssetGas struct {
	outAsset common.Asset
	gas      common.Gas
}
